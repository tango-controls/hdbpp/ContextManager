# Project ContextManager

Maven Java project

This class is able to get context calendar from property.
And manage context changes to update HDB++ manager devices.


## Cloning

```
git clone git@gitlab.com:tango-controls/hdbpp/ContextManager.git
```

## Documentation 

See [Pogo Generated Documentation](https://tango-controls.gitlab.io/hdbpp/ContextManager/doc_html/index.html) for more details.

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* JTango.jar
  

#### Toolchain Dependencies 

* javac 7 or higher
* maven
  

### Build


Instructions on building the project.

```
cd ContextManager
mvn package
```
