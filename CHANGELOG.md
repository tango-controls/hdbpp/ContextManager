# Changelog

#### ContextManager-1.3 - 2022-11-18:
    Update JTangoServer groupId after the move to Sonatype  
    Specify maven-compiler-plugin and maven-jar-plugin Versions in pom.xml

#### ContextManager-1.2 - 2018-10-21:
    Fix a bug in refresh period management.

#### ContextManager-1.1 - 2018-11-07:
    Manager list is taken from database.
    Add a refresh period to send context at all manager devices

#### ContextManager-1.0 - 2017-06-05:
    Initial revision
