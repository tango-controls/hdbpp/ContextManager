//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.contextmanager;

import fr.esrf.Tango.DevFailed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;


/**
 * This class is able to define list of context objects
 *
 * @author verdier
 */

public class ContextList extends ArrayList<Context> {

    //===============================================================
    //===============================================================
    public ContextList(String[] dateStrArray) throws DevFailed {
        for (String line : dateStrArray) {
            add(new Context(line));
        }
        Collections.sort(this, new ContextComparator());
        //  Set ending time for each context
        for (int i=1 ; i<size() ; i++)
            get(i-1).setEndTime(get(i).getStartTime());
    }
    //===============================================================
    //===============================================================
    public Context getCurrentContext() {
        long now = System.currentTimeMillis();
        for (Context context : this) {
            //System.out.println(new Date(now) + " > " + new Date(context.getStartTime()));
            if (now>context.getStartTime() && now<context.getEndTime()) {
                return context;
            }
        }
        //  Not found -> last one
        return get(size()-1);
    }
    //===============================================================
    //===============================================================
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Context context : this)
            sb.append(context).append('\n');
        return sb.toString();
    }
    //===============================================================
    //===============================================================




    //======================================================
    /**
     * Comparators class to sort by type
     */
    //======================================================
    private class ContextComparator implements Comparator<Context> {
        public int compare(Context context1, Context context2) {

            if (context1.getStartTime() > context2.getStartTime())
                return 1;
            else if (context2.getStartTime() > context1.getStartTime())
                return -1;
            else
                return 0;
        }
    }
    //======================================================
    //======================================================
}
