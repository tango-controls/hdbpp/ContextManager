//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.contextmanager;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.Except;

import java.text.SimpleDateFormat;
import java.util.*;


/**
 * This class is able to define a context object
 *
 * @author verdier
 */

public class Context {
    private String name;
    private long startTime;
    private long endTime = 0;
    //===========================================================
    //===========================================================
    public Context(String line) throws DevFailed {
        //  Split date and name
        int idx = line.indexOf(":");
        if (idx<0)
            Except.throw_exception("SyntaxError", line + ": syntax error");
        name = line.substring(0, idx).trim();
        String dateTime = line.substring(idx+1).trim();

        //  Split date and time
        String[] dateAndTime = splitDateAndTime(dateTime);
        List<String> date = splitStr(dateAndTime[0], "/");
        List<String> time = splitStr(dateAndTime[1], ":");
        if (date.size()<3 || time.size()<2)
            Except.throw_exception("SyntaxError", line + ": syntax error");

        //  Get date and time value
        int year = Integer.parseInt(date.get(2));
        if (year<100)
            year += 2000;
        int month = Integer.parseInt(date.get(1)) - 1;
        int day = Integer.parseInt(date.get(0));

        int hour = Integer.parseInt(time.get(0));
        int min = Integer.parseInt(time.get(1));

        Calendar calendar = new GregorianCalendar(year, month, day, hour, min);
        startTime = calendar.getTimeInMillis();
    }
    //===========================================================
    //===========================================================
    public String getName() {
        return name;
    }
    //===========================================================
    //===========================================================
    public long getStartTime() {
        return startTime;
    }
    //===========================================================
    //===========================================================
    public long getEndTime() {
        return endTime;
    }
    //===============================================================
    //===============================================================
    public String getStartingDate() {
        return formatDate(startTime);
    }
    //===============================================================
    //===============================================================
    public String getEndingDate() {
        if (endTime>0)
            return formatDate(endTime);
        else
            return "....";
    }
    //===========================================================
    //===========================================================
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
    //===========================================================
    //===========================================================
    private List<String> splitStr(String str, String separator) throws DevFailed {
        StringTokenizer stk = new StringTokenizer(str, separator);
        List<String> list = new ArrayList<>();
        while (stk.hasMoreTokens())
            list.add(stk.nextToken().trim());
        return list;
    }
    //===========================================================
    //===========================================================
    private String[] splitDateAndTime(String dateStr) throws DevFailed {
        StringTokenizer stk = new StringTokenizer(dateStr);
        if (stk.countTokens()!=2)
            Except.throw_exception("SyntaxError", dateStr + ": syntax error");
        return new String[] { stk.nextToken(), stk.nextToken() };
    }
    //===========================================================
    //===========================================================
    public String toString() {
        String str = name + ": " + new Date(startTime) + " -> ";
        if (endTime==0)
            return str + " ....";
        else
            return str + new Date(endTime);
    }
    //=======================================================
    //=======================================================
    public static String formatDate(long t) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy  HH:mm");
        return simpleDateFormat.format(new Date(t));
    }
    //===========================================================
    //===========================================================
}
